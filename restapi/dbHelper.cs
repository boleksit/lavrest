﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using restapi.Models;
using System.Collections;
using System.Configuration;

namespace restapi
{
    public class dbHelper
    {
        public enum ApiTypes { action, user, list, usersInList };

        public long saveActionQuerry(ApiTypes apiType, object obj)
        {
            MySqlConnection dbConnection;
            dbConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);
            switch (apiType)
            {
                case ApiTypes.action:
                    {
                        ActionItem value = new ActionItem();
                        value = obj as ActionItem;
                        try
                        {
                            dbConnection.Open();
                            long id;
                            string dbQuerry = "INSERT INTO tbItems (tbItems_actionText, tbItems_isDone, tbList_ListId, tbUser_Owner) VALUES ('" + value.actionText + "'," + value.isDone.ToString() + "'," + value.ListId + "'," + value.owner + ");";

                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                cmd.ExecuteNonQuery();
                                id = cmd.LastInsertedId;

                            };

                            return id;
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.user:
                    {
                        User value = new User();
                        value = obj as User;
                        try
                        {
                            dbConnection.Open();
                            long id;
                            string dbQuerry = "INSERT INTO tbUsers (tbUsers_username,tbUsers_displayName,tbUsers_email, tbUsers_googleId) VALUES ('" + value.Username.ToString() + "','" + value.DisplayName + "','" + value.Email + "','" + value.GoogleId + ");";

                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                cmd.ExecuteNonQuery();
                                id = cmd.LastInsertedId;

                            };

                            return id;
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.list:
                    {
                        List value = new List();
                        value = obj as List;
                        try
                        {
                            dbConnection.Open();
                            long id;
                            string dbQuerry = "INSERT INTO tbLists ( tbUsers_ListOwner, tbList_Types_Type) VALUES ('" + value.Owner + "','" + (int)value.ListType + "');";

                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                cmd.ExecuteNonQuery();
                                id = cmd.LastInsertedId;

                            };

                            return id;
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.usersInList:
                    {
                        UsersInList value = new UsersInList();
                        value = obj as UsersInList;
                        try
                        {
                            dbConnection.Open();
                            long id;
                            string dbQuerry = "INSERT INTO tbUsers_in_list (tbLists_ListId, tbUsers_tbUsers_id) VALUES ('" + value.ListId + "'," + value.UserId + ");";

                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                cmd.ExecuteNonQuery();
                                id = cmd.LastInsertedId;

                            };

                            return id;
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
            }
            return 0;
        }



        public long readActionQuerry(UsersInList value)
        {
            MySqlConnection dbConnection;
            dbConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);
            try
            {
                dbConnection.Open();
                long id = 0;
                MySqlDataReader mySqlReader = null;
                string dbQuerry = "SELECT * FROM tbUsers_in_list WHERE tbLists_ListId = " + value.ListId + " AND tbUsers_tbUsers_id =" + value.UserId + ";";

                using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                {

                    mySqlReader = cmd.ExecuteReader();
                    while (mySqlReader.Read())
                    {
                        id = mySqlReader.GetInt64(0);
                    }


                };
                return id;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.Close();
            }
        }







        public object readActionQuerry(ApiTypes apiType, string ID, bool google)
        {
            MySqlConnection dbConnection;
            dbConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);

            switch (apiType)
            {
                case ApiTypes.action:
                    {

                        try
                        {
                            dbConnection.Open();
                            ActionItem action = new ActionItem();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbItems WHERE tbItems_id = " + ID + ";";

                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                mySqlReader = cmd.ExecuteReader();
                                if (mySqlReader.Read())
                                {
                                    action.ID = mySqlReader.GetInt64(0);
                                    action.actionText = mySqlReader.GetString(1);
                                    action.isDone = mySqlReader.GetBoolean(2);
                                    action.ListId = mySqlReader.GetInt64(3);
                                    action.owner = mySqlReader.GetInt32(4);

                                    return action;
                                }
                                else
                                {
                                    return null;
                                }

                            };
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.user:
                    {
                        try
                        {
                            dbConnection.Open();
                            User user = new User();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry;

                            dbQuerry = !google ? "SELECT * FROM tbUsers WHERE tbUsers_id = " + ID + ";" : "SELECT * FROM tbUsers WHERE tbUsers_googleId ='" + ID + "';";

                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                mySqlReader = cmd.ExecuteReader();
                                if (mySqlReader.Read())
                                {
                                    user.UserId = mySqlReader.GetInt32(0);
                                    user.Username = mySqlReader.GetString(1);
                                    user.DisplayName = mySqlReader.GetString(2);
                                    user.Email = mySqlReader.GetString(3);
                                    user.Created = mySqlReader.GetDateTime(4);
                                    user.Modified = mySqlReader.GetDateTime(5);
                                    user.GoogleId = mySqlReader.GetString(6);

                                    return user;
                                }
                                else
                                {
                                    return null;
                                }

                            };
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.list:
                    {

                        try
                        {
                            dbConnection.Open();
                            List list = new List();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbLists WHERE tbLists_id = " + ID + ";";

                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                mySqlReader = cmd.ExecuteReader();
                                if (mySqlReader.Read())
                                {
                                    list.ID = mySqlReader.GetInt64(0);
                                    list.Owner = mySqlReader.GetInt64(1);
                                    list.Created = mySqlReader.GetDateTime(2);
                                    list.Modified = mySqlReader.GetDateTime(3);
                                    list.ListType = (ListTypes)mySqlReader.GetInt32(4);
                                    return list;
                                }
                                else
                                {
                                    return null;
                                }

                            };
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                    //case ApiTypes.usersInList:
                    //    {

                    //        try
                    //        {
                    //            dbConnection.Open();
                    //            UsersInList action = new UsersInList();
                    //            MySqlDataReader mySqlReader = null;
                    //            string dbQuerry = "SELECT * FROM tbUsers_in_list WHERE tbUsers_in_list_id = " + ID + ";";

                    //            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                    //            {

                    //                mySqlReader = cmd.ExecuteReader();
                    //                while (mySqlReader.Read())
                    //                {

                    //                    action.ID=mySqlReader.GetInt64(0);
                    //                    action.ListId = mySqlReader.GetInt64(1);
                    //                    action.UserId = mySqlReader.GetInt64(2);
                    //                }


                    //            };
                    //            return action;
                    //        }
                    //        catch (MySql.Data.MySqlClient.MySqlException ex)
                    //        {
                    //            throw ex;
                    //        }
                    //        finally
                    //        {
                    //            dbConnection.Close();
                    //        }
                    //    }
            }
            return null;

        }
        public List<UsersInList> readAllUserList(long userID)
        {
            MySqlConnection dbConnection;
            dbConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);
            try
            {
                dbConnection.Open();
                List<UsersInList> actionArray = new List<UsersInList>();

                MySqlDataReader mySqlReader = null;
                string dbQuerry = "SELECT * FROM tbUsers_In_List where tbUsers_tbUsers_id" + userID.ToString()+";" ;



                using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                {

                    mySqlReader = cmd.ExecuteReader();
                    while (mySqlReader.Read())
                    {
                        UsersInList temp = new UsersInList();
                        temp.ID = mySqlReader.GetInt64(0);
                        temp.ListId = mySqlReader.GetInt64(1);
                        temp.UserId = mySqlReader.GetInt64(2);

                        actionArray.Add(temp);
                    }


                    return actionArray;

                };
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                dbConnection.Close();
            }
        }
        public ArrayList readAllActionsQuerry(ApiTypes apiType, long userID=0)
        {
            MySqlConnection dbConnection;
            dbConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);
            switch (apiType)
            {
                case ApiTypes.action:
                    {
                        try
                        {
                            dbConnection.Open();
                            ArrayList actionArray = new ArrayList();

                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbItems;";



                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                mySqlReader = cmd.ExecuteReader();
                                while (mySqlReader.Read())
                                {
                                    ActionItem action = new ActionItem();
                                    action.ID = mySqlReader.GetInt64(0);
                                    action.actionText = mySqlReader.GetString(1);
                                    action.isDone = mySqlReader.GetInt16(2) == 1 ? true : false;
                                    action.ListId = mySqlReader.GetInt64(3);
                                    action.owner = mySqlReader.GetInt32(4);
                                    action.created = mySqlReader.GetDateTime(5);
                                    action.modified = mySqlReader.GetDateTime(6);
                                    actionArray.Add(action);
                                }


                                return actionArray;

                            };
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.user:
                    {
                        try
                        {
                            dbConnection.Open();
                            ArrayList userArray = new ArrayList();

                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbUsers;";



                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                mySqlReader = cmd.ExecuteReader();
                                while (mySqlReader.Read())
                                {
                                    User user = new User();
                                    user.UserId = mySqlReader.GetInt32(0);
                                    user.Username = mySqlReader.GetString(1);
                                    user.DisplayName = mySqlReader.GetString(2);
                                    user.Email = mySqlReader.GetString(3);
                                    user.Created = mySqlReader.GetDateTime(4);
                                    user.Modified = mySqlReader.GetDateTime(5);
                                    user.GoogleId = mySqlReader.GetString(6);
                                    userArray.Add(user);
                                }


                                return userArray;

                            };
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.list:
                    {
                        try
                        {
                            dbConnection.Open();
                            ArrayList listArray = new ArrayList();

                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbLists;";



                            using (MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection))
                            {

                                mySqlReader = cmd.ExecuteReader();
                                while (mySqlReader.Read())
                                {
                                    List list = new List();
                                    list.ID = mySqlReader.GetInt64(0);
                                    list.Owner = mySqlReader.GetInt64(1);
                                    list.Created = mySqlReader.GetDateTime(2);
                                    list.Modified = mySqlReader.GetDateTime(3);
                                    list.ListType = (ListTypes)mySqlReader.GetInt32(4);
                                    listArray.Add(list);
                                }


                                return listArray;

                            };
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
            }
            return null;
        }
        public bool deleteActionQuerry(ApiTypes apiType, long ID, UsersInList usersInList = null)
        {
            MySqlConnection dbConnection;
            dbConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);

            switch (apiType)
            {
                case ApiTypes.action:
                    {
                        try
                        {
                            dbConnection.Open();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbItems WHERE tbItems_id = " + ID.ToString() + ";";


                            MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection);
                            mySqlReader = cmd.ExecuteReader();



                            if (mySqlReader.Read())
                            {
                                mySqlReader.Close();

                                dbQuerry = "DELETE FROM tbItems WHERE tbItems_id = " + ID.ToString() + ";";
                                cmd = new MySqlCommand(dbQuerry, dbConnection);
                                cmd.ExecuteNonQuery();

                                return true;
                            }
                            else
                            {

                                return false;
                            }

                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.user:
                    {
                        try
                        {
                            dbConnection.Open();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbUsers WHERE tbUsers_id = " + ID.ToString() + ";";


                            MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection);
                            mySqlReader = cmd.ExecuteReader();



                            if (mySqlReader.Read())
                            {
                                mySqlReader.Close();

                                dbQuerry = "DELETE FROM tbUsers WHERE tbUsers_id = " + ID.ToString() + ";";
                                cmd = new MySqlCommand(dbQuerry, dbConnection);
                                cmd.ExecuteNonQuery();

                                return true;
                            }
                            else
                            {

                                return false;
                            }

                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.list:
                    {
                        try
                        {
                            dbConnection.Open();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbLists WHERE tbLists_id = " + ID.ToString() + ";";


                            MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection);
                            mySqlReader = cmd.ExecuteReader();



                            if (mySqlReader.Read())
                            {
                                mySqlReader.Close();

                                dbQuerry = "DELETE FROM tbLists WHERE tbLists_id = " + ID.ToString() + ";";
                                cmd = new MySqlCommand(dbQuerry, dbConnection);
                                cmd.ExecuteNonQuery();

                                return true;
                            }
                            else
                            {

                                return false;
                            }

                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.usersInList:
                    {
                        try
                        {
                            dbConnection.Open();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbUsers_in_list WHERE tbLists_ListId = " + usersInList.ListId + " and tbUsers_tbUsers_id=" + usersInList.UserId + ";";


                            MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection);
                            mySqlReader = cmd.ExecuteReader();



                            if (mySqlReader.Read())
                            {
                                long id = mySqlReader.GetInt64(0);
                                mySqlReader.Close();

                                dbQuerry = "DELETE FROM tbUsers_in_list WHERE tbUsers_in_list_id = " + id + ";";
                                cmd = new MySqlCommand(dbQuerry, dbConnection);
                                cmd.ExecuteNonQuery();

                                return true;
                            }
                            else
                            {

                                return false;
                            }

                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
            }
            return false;

        }
        public bool updateActionQuerry(ApiTypes apiType, long ID, object obj)
        {
            MySqlConnection dbConnection;
            dbConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["localDB"].ConnectionString);
            switch (apiType)
            {
                case ApiTypes.action:
                    {
                        ActionItem value = new ActionItem();
                        value = obj as ActionItem;

                        try
                        {
                            dbConnection.Open();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbItems WHERE tbItems_id= " + ID.ToString() + ";";

                            MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection);
                            mySqlReader = cmd.ExecuteReader();

                            if (mySqlReader.Read())
                            {
                                mySqlReader.Close();

                                dbQuerry = "UPDATE tbItems SET tbItems_actionText='" + value.actionText + "', tbItems_isDone ='" + value.isDone.ToString() + "', tbList_ListId='" + value.ListId + "', tbUser_Owner='" + value.owner + "' WHERE tbItems_id='" + value.ID.ToString() + "';";
                                cmd = new MySqlCommand(dbQuerry, dbConnection);
                                cmd.ExecuteNonQuery();

                                return true;
                            }
                            else
                            {

                                return false;
                            }
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.user:
                    {
                        User value = new User();
                        value = obj as User;
                        try
                        {
                            dbConnection.Open();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbUsers WHERE tbUsers_id= " + ID.ToString() + ";";

                            MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection);
                            mySqlReader = cmd.ExecuteReader();

                            if (mySqlReader.Read())
                            {
                                mySqlReader.Close();

                                dbQuerry = "UPDATE tbUsers SET tbUsers_id='" + value.UserId + "', tbUsers_username ='" + value.Username.ToString() + "', tbUsers_displayName ='" + value.DisplayName + "', tbUsers_email ='" + value.Email + "', tbUsers_googleId='" + value.GoogleId + "' WHERE tbUsers_id='" + value.UserId + "';";
                                cmd = new MySqlCommand(dbQuerry, dbConnection);
                                cmd.ExecuteNonQuery();

                                return true;
                            }
                            else
                            {

                                return false;
                            }
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }
                case ApiTypes.list:
                    {
                        List value = new List();
                        value = obj as List;

                        try
                        {
                            dbConnection.Open();
                            MySqlDataReader mySqlReader = null;
                            string dbQuerry = "SELECT * FROM tbLists WHERE tbLists_id= " + ID.ToString() + ";";

                            MySqlCommand cmd = new MySqlCommand(dbQuerry, dbConnection);
                            mySqlReader = cmd.ExecuteReader();

                            if (mySqlReader.Read())
                            {
                                mySqlReader.Close();

                                dbQuerry = "UPDATE tbLists SET tbLists_id='" + ID + "', tbUsers_ListOwner ='" + value.Owner + "', tbList_Types_Type='" + (int)value.ListType + "' WHERE tbLists_id='" + ID + "';";
                                cmd = new MySqlCommand(dbQuerry, dbConnection);
                                cmd.ExecuteNonQuery();

                                return true;
                            }
                            else
                            {

                                return false;
                            }
                        }
                        catch (MySql.Data.MySqlClient.MySqlException ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            dbConnection.Close();
                        }
                    }

            }
            return false;

        }
    }
}