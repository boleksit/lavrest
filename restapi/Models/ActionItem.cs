﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace restapi.Models
{
	public class ActionItem
	{
        public long ID { get; set; }
        public bool isDone { get; set; }
        public string actionText { get; set; }
        public long ListId { get; set; }
        public int owner { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
    }
}