﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace restapi.Models
{
    public enum ListTypes { ToDo=1, Shopping=2 };
    public class List
    {
        public long ID { get; set; }
        public long Owner { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public ListTypes ListType { get; set; }


    }

}