﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace restapi.Models
{
    public class UsersInList
    {
        public long ID { get; set; }
        public long ListId { get; set; }
        public long UserId { get; set; }

        public UsersInList(long ListId, long UserId)
        {
            this.ListId = ListId;
            this.UserId = UserId;
        }
        public UsersInList()
        {

        }
    }
}