﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace restapi.Models
{
   public  class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string GoogleId { get; set; }

       

        
    }
}