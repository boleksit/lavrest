﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using restapi.Models;
using System.Diagnostics;
using System.Collections;


namespace restapi.Controllers
{
    public class usersInListController : ApiController
    {
       
        private dbHelper db;
        public usersInListController ()
        {
            db = new dbHelper ();
        }
        /// <summary>
        /// Get all lists
        /// </summary>
        // GET: api/usersInList
        //public ArrayList Get()
        //{
                      
        //    return db.readAllActionsQuerry(dbHelper.ApiTypes.usersInList);             
        //}
        /// <summary>
        /// Get specific list by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/usersInList/5
        public List<UsersInList> Get(long id,bool lists=false)
        {
            List<UsersInList> user = new List<UsersInList>();
            if (!lists)
            { user.Add(db.readActionQuerry(dbHelper.ApiTypes.usersInList, id.ToString(), false) as UsersInList); }
            else
            {
                user = db.readAllUserList(id);
            }
            if(user==null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return user;
        }
        // GET: api/usersInList/{ListId}/{UserId}
        public long Get(long ListId = 0, long UserId = 0)
        {
            
            long userId = db.readActionQuerry(new UsersInList(ListId, UserId));
            if (userId == 0)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return userId;
        }
        /// <summary>
        /// Add new list
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // POST: api/usersInList
        public HttpResponseMessage Post([FromBody]UsersInList value)
        {
           
            long id;
            id=db.saveActionQuerry(dbHelper.ApiTypes.usersInList,value);
           
           

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Request.RequestUri, string.Format("{0}", id));
           
            return response;

        }

        /// <summary>
        /// Delete list
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/usersInList/5
        public HttpResponseMessage Delete(long ListId, long UserId)
        {
            bool recordExist = false;
            UsersInList value = new UsersInList(ListId, UserId);
            recordExist = db.deleteActionQuerry(dbHelper.ApiTypes.usersInList,0,value);

            HttpResponseMessage response;

            if (recordExist)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
        }
    }
}
