﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using restapi.Models;
using System.Diagnostics;
using System.Collections;


namespace restapi.Controllers
{
    public class actionController : ApiController
    {
       
        private dbHelper db;
        public actionController ()
        {
            db = new dbHelper ();
        }
        /// <summary>
        /// Get all actions
        /// </summary>
        // GET: api/action
        public ArrayList Get()
        {
                      
            return db.readAllActionsQuerry(dbHelper.ApiTypes.action);             
        }
        /// <summary>
        /// Get specific action by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/action/5
        public ActionItem Get(long id)
        {
            ActionItem action = new ActionItem();
            action=db.readActionQuerry(dbHelper.ApiTypes.action,id.ToString(),false) as ActionItem;
            if(action==null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return action;
        }
        /// <summary>
        /// Add new action
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // POST: api/action
        public HttpResponseMessage Post([FromBody]ActionItem value)
        {

            long id;
            id=db.saveActionQuerry(dbHelper.ApiTypes.action,value);
           
           

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Request.RequestUri, string.Format("{0}", id));

            return response;

        }
        /// <summary>
        /// Update action
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // PUT: api/action/5
        public HttpResponseMessage Put(long id, [FromBody]ActionItem value)
        {
            bool recordExist = false;
            recordExist = db.updateActionQuerry(dbHelper.ApiTypes.action,id, value);

            HttpResponseMessage response;

            if (recordExist)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
        }
        /// <summary>
        /// Delete action
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/action/5
        public HttpResponseMessage Delete(long id)
        {
            bool recordExist = false;
            recordExist = db.deleteActionQuerry(dbHelper.ApiTypes.action,id);

            HttpResponseMessage response;

            if (recordExist)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
        }
    }
}
