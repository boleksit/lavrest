﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using restapi.Models;
using System.Diagnostics;
using System.Collections;


namespace restapi.Controllers
{
    public class listsController : ApiController
    {
       
        private dbHelper db;
        public listsController ()
        {
            db = new dbHelper ();
        }
        /// <summary>
        /// Get all lists
        /// </summary>
        // GET: api/lists
        public ArrayList Get()
        {
                      
            return db.readAllActionsQuerry(dbHelper.ApiTypes.list);             
        }
        /// <summary>
        /// Get specific list by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/lists/5
        public List Get(long id)
        {
            List list = new List();
            list=db.readActionQuerry(dbHelper.ApiTypes.list,id.ToString(),false) as List;
            if(list==null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return list;
        }
        /// <summary>
        /// Add new list
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // POST: api/lists
        public HttpResponseMessage Post([FromBody]List value)
        {

            long id;
            id=db.saveActionQuerry(dbHelper.ApiTypes.list,value);
           
           

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Request.RequestUri, string.Format("{0}", id));

            return response;

        }
        /// <summary>
        /// Update list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // PUT: api/lists/5
        public HttpResponseMessage Put(long id, [FromBody]List value)
        {
            bool recordExist = false;
            recordExist = db.updateActionQuerry(dbHelper.ApiTypes.list,id, value);

            HttpResponseMessage response;

            if (recordExist)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
        }
        /// <summary>
        /// Delete list
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/lists/5
        public HttpResponseMessage Delete(long id)
        {
            bool recordExist = false;
            recordExist = db.deleteActionQuerry(dbHelper.ApiTypes.list,id);

            HttpResponseMessage response;

            if (recordExist)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
        }
    }
}
