﻿using restapi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace restapi.Controllers
{
    public class usersController : ApiController
    {

        private dbHelper db;
        public usersController()
        {
            db = new dbHelper();
        }
        /// <summary>
        /// Get all users
        /// </summary>
        // GET: api/action
        public ArrayList Get()
        {

            return db.readAllActionsQuerry(dbHelper.ApiTypes.user);
        }
        /// <summary>
        /// Get specific user by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/users/5
        public User Get(string id, [FromUri] bool google=false)
        {
            User user = new User();
            
                user = db.readActionQuerry(dbHelper.ApiTypes.user, id,google) as User;
            
                if (user == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return user;
        }
        /// <summary>
        /// Add new action
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        // POST: api/action
        public HttpResponseMessage Post([FromBody]User value)
        {

            long id;
            id = db.saveActionQuerry(dbHelper.ApiTypes.user,value);



            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Request.RequestUri, string.Format("{0}", id));

            return response;

        }
        /// <summary>
        /// Update action
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // PUT: api/action/5
        public HttpResponseMessage Put(long id, [FromBody]User value)
        {
            bool recordExist = false;
            recordExist = db.updateActionQuerry(dbHelper.ApiTypes.user,id, value);

            HttpResponseMessage response;

            if (recordExist)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
        }
        /// <summary>
        /// Delete action
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/action/5
        public HttpResponseMessage Delete(long id)
        {
            bool recordExist = false;
            recordExist = db.deleteActionQuerry(dbHelper.ApiTypes.user,id);

            HttpResponseMessage response;

            if (recordExist)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
                return response;

            }
        }
    }
}
